﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace firstApp
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = 0;
            int num2 = 0;
            int num3 = 0;
            string output = "";

            Console.WriteLine("Enter the first number");
            num1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the second number");
            num2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the third number");
            num3 = Convert.ToInt32(Console.ReadLine());

            if (num1 > num2 && num1 > num3)
            {
                if (num2 > num3)
                {
                    output = num3 + "-----" + num2 + "-----" + num1;
                }
                else
                {
                    output = num2 + "-----" + num3 + "-----" + num1;
                }
            }
            else if (num2 > num1 && num2 > num3)
            {
                if (num1 > num3)
                {
                    output = num3 + "-----" + num1 + "-----" + num2;
                }
                else
                {
                    output = num1 + "-----" + num3 + "-----" + num2;
                }
            }
            else if (num3 > num2 && num3 > num1)
            {
                if (num2 > num1)
                {
                    output = num1 + "-----" + num2 + "-----" + num3;
                }
                else
                {
                    output = num2 + "-----" + num1 + "-----" + num3;
                }
            }
            else
            {
                output = "There was an error";
            }

            Console.WriteLine("The result of the inputs you entered arranged in an ascending order is {0}",output);
            Console.ReadLine();
        }
    }
}
