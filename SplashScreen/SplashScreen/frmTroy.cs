﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace SplashScreen
{
    public partial class frmTroy : Form
    {
        public frmTroy()
        {
            Thread T = new Thread(new ThreadStart(StartForm));
            T.Start();
            Thread.Sleep(15000);
            InitializeComponent();
            T.Abort();
        }

        public void StartForm()
        {
            Application.Run(new frmSplash());
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            lblText.Text = "hello world";
        }
    }
}
