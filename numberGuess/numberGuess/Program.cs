﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace numberGuess
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numberArray = new int[5];
            ArrayPractise(numberArray);
        }

        public static void NumberGuess()
        {
            int myNum = 6;
            int userNum = 0;
            int guessCount = 0;
            string output = "";

            do
            {
                Console.WriteLine("Can you guess my number (1 - 10)?");
                userNum = Int32.Parse(Console.ReadLine());
                if (userNum == myNum)
                {
                    output = "Your guess " + userNum + " is correct!!! Congratulations!";
                }
                else
                {
                    output = "Your guess " + userNum + " is incorrect, please try again";
                }
                Console.WriteLine(output);
                guessCount++;
            }
            while (userNum != myNum);

            Console.WriteLine("You guessed " + guessCount + " times");
            Console.ReadLine();
        }

        public static void StorePurchases()
        {
            decimal purchase = 0;
            decimal total = 0;

            Console.WriteLine("Welcome to the store purchase calculator, please enter 10 purchases");
            Console.WriteLine("");

            for (int i = 0; i < 10; i ++)
            {
                Console.WriteLine("Enter the cost of purchase " + (i + 1));
                purchase = Int32.Parse(Console.ReadLine());
                total += purchase;
                Console.WriteLine("");
                Console.WriteLine("Purchase: " + (i + 1) + " is $" + purchase);
                Console.WriteLine("");
                Console.WriteLine("");
            }

            Console.WriteLine("");
            Console.WriteLine("Total: $" + total);
            Console.ReadLine();

        }

        public static void ArrayPractise (int[] numberArray)
        {
            Console.WriteLine("Enter 5 numbers");
            for (int i = 0 ; i < 5 ; i++)
            {
                Console.WriteLine("Enter number " + (i + 1) + " (Array index " + i + " )");
                numberArray[i] = Int32.Parse(Console.ReadLine());
            }

            Console.WriteLine("Numbers displayed as entered");
            foreach (int x in numberArray)
            {
                Console.WriteLine(x);
            }

            Console.WriteLine("Numbers displayed in reverse");
            for (int j = 4 ; j >= 0 ; j--)
            {
                Console.WriteLine(numberArray[j]);
            }

            Console.ReadLine();
        }
    }   
    
}
