﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practise
{   
    class SignOn
    {   
        private string userName = "";
        private int hourlyRate = 0;
        private string jobName = "";
        private int signOnTime = 0;
        private int signOffTime = 0;
        private int lunchStart = 0;
        private int lunchFinish = 0;
        private int dailyLunchTotal = 0;
        private int dailyWorkAmount = 0;
        private int dailyWorkTotalQty = 0;
        private string dailyOutput = "";
        private string [] weeklyOutput = new string [999];
        public static int j = 0;

        public string _userName { get { return userName; } set { userName = value; } }
        public int _hourlyRate { get { return hourlyRate; } set { hourlyRate = value; } }
        public string _jobName { get { return jobName; } set { jobName = value; } }
        public int _signOnTime { get { return signOnTime; } set { signOnTime = value; } }
        public int _signOffTime { get { return signOffTime; } set { signOffTime = value; } }
        public int _lunchStart { get { return lunchStart; } set { lunchStart = value; } }
        public int _lunchFinish { get { return lunchFinish; } set { lunchFinish = value; } }

        public int _dailyLunchTotal()
        {
            return _lunchFinish - _lunchStart;
        }

        public int _dailyWorkAmount()
        {
            return _signOffTime - _signOnTime;
        }

        public int _dailyWorkTotalQty ()
        {
            return _dailyWorkAmount() - _dailyLunchTotal();
        }

        public int _dailyWorkTotal ()
        {
            return (_dailyWorkTotalQty() / 60) * _hourlyRate;
        }


        public string _dailyOutput ()
        {
            return "\nTodays results are" + "\n--------------------" + "\nName: " + _userName + "\nJob name: " + _jobName + "\nSign on time: " + _signOnTime +
                    "\nSign off time: " + _signOffTime + "\nLunch break length: " + _dailyLunchTotal() + "\n--------------------" + "\nHourly rate: $" + _hourlyRate +
                    "\nTotal hours worked minus lunch break: " + (_dailyWorkTotalQty()/60) + "\nTotal pay amount for day: $" + _dailyWorkTotal();
        }

        public string _weeklyOutput ()
        {
            weeklyOutput[j] = _dailyOutput();
            j++;
            return weeklyOutput[j - 1];
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            SignOn monday = new SignOn();
            Console.WriteLine("Enter your name:");
            monday._userName = Console.ReadLine();
            Console.WriteLine("Enter your hourly rate:");
            monday._hourlyRate = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the job name:");
            monday._jobName = Console.ReadLine();
            Console.WriteLine("Enter the sign on time in 24 hour format (eg 2230):");
            monday._signOnTime = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the time that you started your lunch break in 24 hour format (eg 2230):");
            monday._lunchStart = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the time that you finished your lunch break in 24 hour format (eg 2230):");
            monday._lunchFinish = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the sign off time in 24 hour format (eg 2230):");
            monday._signOffTime = Int32.Parse(Console.ReadLine());
            monday._weeklyOutput();
            
            Console.WriteLine("Would you like to see the results for the day? (y = yes, n = no)");
            string resultConfirm = Console.ReadLine();

            if (resultConfirm == "y")
            {
                Console.WriteLine(monday._dailyOutput());
                Console.WriteLine(monday._weeklyOutput());
            }
            else
            {
                

            }


            Console.ReadLine();








        }
    }
}
