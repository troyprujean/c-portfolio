﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassPractise3
{   
    class Calc
    {
        private int num1 = 0;
        private int num2 = 0;
        private int num3 = 0;

        public int _num1 { get { return num1; } set { num1 = value; } }
        public int _num2 { get { return num2; } set { num2 = value; } }
        public int _num3 { get { return num3; } set { num3 = value; } }

        public int CalcAdd ()
        {
            return num1 + num2 + num3;
        }

        public int CalcMult()
        {
            return (num1 * num2) * num3;
        }

        public int CalcAvg()
        {
            return (num1 + num2 + num3) / 3;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Calc test1 = new Calc();
            test1._num1 = 5;
            test1._num2 = 10;
            test1._num3 = 20;
            Console.WriteLine("Calculator");
            Console.WriteLine("1st number: " + test1._num1 + "\n2nd number: " + test1._num2 + "\n3rd number: " + test1._num3);
            Console.WriteLine("Addition of these numbers results in: " + test1.CalcAdd());
            Console.WriteLine("Multiplication of these numbers results in: " + test1.CalcMult());
            Console.WriteLine("The average of theses numbers is: " + test1.CalcAvg());
            Console.ReadLine();
           
        }
    }
}
