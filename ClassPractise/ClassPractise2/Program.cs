﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassPractise2
{
    class Student
    {
        private string studentID = "";
        private string firstName = "";
        private string lastName = "";
        private int phoneNumber = 0;
        private string address = "";
        private string email = "";


        public string _studentID { get { return studentID; } set { studentID = value; } }
        public string _firstName { get { return firstName; } set { firstName = value; } }
        public string _lastName { get { return lastName; } set { lastName = value; } }
        public int _phoneNumber { get { return phoneNumber; } set { phoneNumber = value; } }
        public string _address { get { return address; } set { address = value; } }
        public string _email { get { return email; } set { email = value; } }

        public string StudentDetails ()
        {
            return "STUDENT DETAILS" + "\n---------------" + "\nStudent ID: " + studentID + "\nName: " + firstName + " " + lastName
                    + "\nPhone: " + phoneNumber + "\nAddress: " + address + "\nEmail: " + email;
        }
        
        // Constructor method to assign the values of test1 to the private class variables
        public Student (string id, string fname, string lname, int ph, string _address, string _email)
        {
            studentID = id;
            firstName = fname;
            lastName = lname;
            phoneNumber = ph;
            address = _address;
            email = _email;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Student test1 = new Student("9930285", "Troy", "Prujean", 0221998021, "42 Otumoetai Rd", "9930285@student.toiohomai.ac.nz");
           
            /*Console.WriteLine("Enter the students ID number");
            test1._studentID = Console.ReadLine();
            Console.WriteLine("Enter the students first name");
            test1._firstName = Console.ReadLine();
            Console.WriteLine("Enter the students last name");
            test1._lastName = Console.ReadLine();
            Console.WriteLine("Enter the students phone number");
            test1._phoneNumber = Int32.Parse(Console.ReadLine());
            Console.WriteLine("Enter the students address");
            test1._address = Console.ReadLine();
            Console.WriteLine("Enter the students email address");
            test1._email = Console.ReadLine();
            Console.WriteLine();*/

            Console.WriteLine(test1.StudentDetails());
            Console.ReadLine();
        }
    }
}
