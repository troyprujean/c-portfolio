﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassPractise
{
    class Animal
    {
        private string sound = "";
        private string name = "";

        //Constructor function for sound
        public string _sound
        {
            get
            {
                return sound;
            }
            set
            {
                sound = value;
            }
        }

        //Constructor function for name
        public string _name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string AnimalSays ()
        {
            return "The " + _name + " says " + _sound;
        }

        
    }


    class Program
    {
        static void Main(string[] args)
        {
            //Creates new instance of Animal class
            Animal test1 = new Animal();
            Console.WriteLine("Enter the name of an animal");
            test1._name = Console.ReadLine();
            Console.WriteLine("Enter the sound the animal makes");
            test1._sound = Console.ReadLine();
            Console.WriteLine(test1._name + " makes the sound of: " + test1._sound);
            Console.WriteLine(test1.AnimalSays());
            Console.ReadLine();

        }
    }
}
